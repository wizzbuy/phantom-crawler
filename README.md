phantom-crawler
===============

An expressive Crawler library for PhantomJS.


Documentation
-------------

Currently lacking; see example files to get started.


Third-Party Libraries
---------------------

The following three third-party libraries are needed:

- async.js
- jquery.js
- underscore.js

They are not included in this repository and can be fetched automatically by
running:

    make deps

phantom.injectJs("crawler/crawler.js");

/*
 * Extract recent post titles and save to a file.
 */

new Crawler('http://lucumr.pocoo.org')
  .chrome()
  .jQuery()
  .crawl(function () {
    return $('.entry-overview h1 a').map(function () {
      return $(this).text().trim();
    }).toArray();
  })
  .save('lucumr.pocoo.org-recent-posts.json', true)
  .exec();

phantom.injectJs("crawler/crawler.js");

/*
 * Crawl pages two links deep past the entry page and extract post titles and
 * dates.
 */

new Crawler('http://lucumr.pocoo.org/archive/')
  .debug()
  .opera()
  .jQuery()
  .follow('.blog-archive li ul li a')
  .follow('.blog-archive a')
  .crawl(function () {
    return {
      tilte: $('.title').text().trim(),
      date: $('.date').text().trim().slice(11)
    };
  })
  .dump()
  .exec();

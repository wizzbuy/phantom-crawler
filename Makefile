PHANTOMJS=phantomjs
LINT=jsl

ASYNC=https://raw.github.com/caolan/async/master/lib/async.js
JQUERY=http://code.jquery.com/jquery-1.10.1.js
UNDERSCORE=http://underscorejs.org/underscore.js

SUITE=$(wildcard *.js)

.PHONY: *.js

default: test

deps:
	mkdir -p crawler/lib
	curl $(ASYNC) --progress-bar -o crawler/lib/async.js
	curl $(JQUERY) --progress-bar -o crawler/lib/jquery.js
	curl $(UNDERSCORE) --progress-bar -o crawler/lib/underscore.js

test: lint $(SUITE)

lint: 
	$(LINT) -process crawler/crawler.js

clean:
	rm -rf crawler/lib
	rm -f lucumr.pocoo.org-recent-posts.json

*.js:
	$(PHANTOMJS) $@

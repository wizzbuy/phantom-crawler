phantom.injectJs("crawler/crawler.js");

/*
 * Crawl pages one link deep past the entry page and extract post titles and
 * dates.
 */

new Crawler('http://lucumr.pocoo.org/')
  .debug()
  .firefox()
  .jQuery()
  .follow('.entry-overview a')
  .crawl(function () {
    // Yes, that information is available directly on the main page but it's
    // just to demonstrate simple navigation.
    return {
      tilte: $('.title').text().trim(),
      date: $('.date').text().trim().slice(11)
    };
  })
  .dump()
  .exec();

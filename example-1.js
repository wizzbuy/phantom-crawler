phantom.injectJs("crawler/crawler.js");

/*
 * Extract post title and date.
 */

new Crawler('http://lucumr.pocoo.org/2014/1/9/ucs-vs-utf8/')
  .debug()
  .jQuery()
  .underscore()
  .crawl(function () {
    return {
      tilte: $('.title').text().trim(),
      date: $('.date').text().trim().slice(11)
    };
  })
  .dump()
  .exec();

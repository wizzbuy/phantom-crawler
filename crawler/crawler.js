var system = require('system');
var webpage = require('webpage');
var webserver = require('webserver');
var fs = require('fs');

var AGENT_FIREFOX = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0';
var AGENT_OPERA = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36';
var AGENT_CHROME = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36 OPR/18.0.1284.63';

phantom.injectJs('crawler/lib/async.js');
phantom.injectJs('crawler/lib/underscore.js');

var jquery = '//code.jquery.com/jquery-1.10.1.min.js';

function Crawler (urls, options) {
  if (typeof urls === 'undefined')
    urls = [];

  if (typeof urls === 'string')
    urls = [urls];

  var defaults = {
    // Whether or not to print debugging information.
    debug: false,

    // Whether or not to automatically inject jQuery.
    jquery: false,

    // Whether or not to automatically inject underscore.
    underscore: false,

    // Maximum number of concurrent crawling happening at any given time.
    // Currently disabled to an odd race condition when used.
    concurrency: 5,

    // Default user agent.
    agent: AGENT_FIREFOX,

    // Blacklisted routes
    deny: [
      'robots.txt',
      'index.html',
      'favicon.ico'
    ],

    // Allowed origins (for CORS). PhantomJS seemingly doesn't support OPTIONS
    // requests so using allow() is currently ineffective.
    origins: []
  };

  this.options = _.extend({}, defaults, options || {});

  // URLs to crawl for the next operation.
  this.urls = urls;

  // Stack of operations to perform.
  this.stack = [];

  // Crawled data store.
  this.data = [];

  // Errors store.
  this.errors = [];
}

Crawler.prototype.jQuery = function () {
  this.options.jquery = true;
  return this;
};

Crawler.prototype.underscore = function () {
  this.options.underscore = true;
  return this;
};

Crawler.prototype.debug = function () {
  this.options.debug = true;
  return this;
};

Crawler.prototype.dump = function () {
  this.stack.push({
    operation: 'dump'
  });

  return this;
};

Crawler.prototype.agent = function (agent) {
  this.options.agent = agent;
  return this;
};

Crawler.prototype.firefox = function () {
  return this.agent(AGENT_FIREFOX);
};

Crawler.prototype.opera = function () {
  return this.agent(AGENT_OPERA);
};

Crawler.prototype.chrome = function () {
  return this.agent(AGENT_CHROME);
};

Crawler.prototype.follow = function (fn, attr) {
  if (typeof fn === 'string') {
    fn = 'function () {' +
      'return $("' + fn + '").map(function () {' +
        'return this["' + (attr || 'href') + '"];' +
      '}).toArray();' +
    '}';
  }

  this.stack.push({
    operation: 'follow',
    handler: fn
  });

  return this;
};

Crawler.prototype.crawl = function (fn, max) {
  this.stack.push({
    operation: 'crawl',
    handler: fn,
    max: max
  });

  return this;
};

Crawler.prototype.format = function (pretty) {
  return JSON.stringify({
    date: (new Date()).toUTCString(),
    errors: this.errors,
    data: this.data
  }, null, +pretty);
};

Crawler.prototype.exec = function (urls, processed) {
  if (_.isFunction(urls)) {
    processed = urls;
    urls = null;
  }

  if (typeof urls === 'string')
    urls = [urls];

  var exit = urls === undefined;
  urls = urls || this.urls;

  var self = this;

  if (this.options.debug) {
    console.log('Operations:');
    console.log(JSON.stringify(this.stack, null, 2));
  }

  async.eachSeries(this.stack, function (op, next) {

    if (self.options.debug) {
      console.log('Operation: ' + op.operation);
    }

    if (op.operation == 'dump') {
      console.log(self.format(true));
      next();
    }
    else if (op.operation == 'save') {
      fs.write(op.filename, self.format(op.pretty), 'w');
      next();
    }
    else {
      if (!urls || !urls.length) {
        next();
      }
      else {

        var tmp = urls.slice(0, op.max || urls.length);
        urls = [];

        async.eachSeries(tmp, function iterator (url, done) {

          if (self.options.debug) {
            console.log('Visiting: ' + url);
          }

          var page = webpage.create();

          page.settings.userAgent = self.options.agent;

          function process () {
            var data = page.evaluate(op.handler);

            switch (op.operation) {
              case 'follow':
                urls = urls.concat(data || []);
                break;

              case 'crawl':
                self.data.push({
                  url: url,
                  date: (new Date()).toUTCString(),
                  data: data
                });

                break;

              default:
                break;
            }

            page.close();

            done();
          }

          page.onError = function (msg, trace) {
            self.errors.push({
              url: url,
              date: (new Date()).toUTCString(),
              message: msg,
              trace: trace
            });

            if (self.options.debug) {
              console.log('Error: ' + msg);
            }
          };

          page.open(url, function () {
            if (self.options.jquery === true) {
              page.injectJs('crawler/lib/jquery.js');
            }

            if (self.options.underscore === true) {
              page.injectJs('crawler/lib/underscore.js');
            }

            process();
          });

        }, function callback () {

          next();

        });

      }

    }

  }, function () {

    if (_.isFunction(processed)) {
      processed(self.errors.length);
    }
    else if (exit) {
      phantom.exit(self.errors.length);
    }

  });

  return this;
};

Crawler.prototype.save = function (filename, pretty) {
  this.stack.push({
    operation: 'save',
    filename: filename,
    pretty: pretty
  });

  return this;
};

Crawler.prototype.clone = function () {
  var instance = new Crawler([], this.options);
  instance.stack = this.stack;

  return instance;
};

Crawler.prototype.allow = function (origin) {
  this.options.origins.push(origin);
  return this;
};

Crawler.prototype.serve = function (port) {
  if (typeof port === 'string') {
    port = parseInt(port, 10);
  }

  var self = this;

  var origins = self.options.origins;

  var server = webserver.create();
  var service = server.listen(port, function (req, res) {

    var url = decodeURIComponent(req.url.slice(1));

    if (self.options.deny.indexOf(url) !== -1) {
      res.statusCode = 404;
      res.write('Not found.');
      res.close();
    }
    else if (origins.length && origins.indexOf(req.headers.Origin) === -1) {
      res.statusCode = 400;
      res.write('Bad request.');
      res.close();
    }
    else {

      // TODO Automatically add http:// to url if missing

      var crawler = self.clone();

      crawler.exec(url, function (err) {

        var data = crawler.format(true);

        res.statusCode = 200;
        res.headers = {
          'Content-Type': 'application/json;charset=utf-8',
          'Content-Length': data.length
        };

        if (req.headers.Origin) {
          res.headers['Access-Control-Allow-Origin'] = req.headers.Origin;
        }

        res.write(data);
        res.close();

      });
    }

  });

  return this;
};

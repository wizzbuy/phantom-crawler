phantom.injectJs("crawler/crawler.js");

/*
 * Palindromes are good practice.
 */

new Crawler('http://headers.jsontest.com/')
  .agent('sexe vêtu, tu te vexes')
  .crawl(function () {
    return document.body.innerText;
  })
  .dump()
  .exec();

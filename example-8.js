phantom.injectJs("crawler/crawler.js");

new Crawler()
  .chrome()
  .jQuery()
  .crawl(function () {
    return $('img').map(function () {
      return $(this).attr('src');
    }).toArray();
  })
  .allow('http://beta.madamedemande.com')
  .serve(require('system').env['PORT'] || 8888);

phantom.injectJs("crawler/crawler.js");

/*
 * Extract titles from top navigation pages.
 */

new Crawler('http://lucumr.pocoo.org')
  .debug()
  .jQuery()
  .follow(function () {
    return $('.navigation a').map(function () {
      return this.href;
    }).toArray();
  })
  .crawl(function () {
    return document.title;
  })
  .dump()
  .exec();
